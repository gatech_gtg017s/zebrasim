#!/bin/bash
ld -verbose |awk 'BEGIN{f=0}{if (f) f++};/========.*/{if (f==0) {f=1;} else {f=0}};{if (f>1) {print}}' | awk '/.*__bss_start/{\
print $0; \
print "  target_bss : { . = .; __start_target_bss = .; libzebrasim-greybox.a(.bss); __stop_target_bss = .; }"; \
next \
}{print $0}' | awk '/.*[.]data[ ]*:/{ \
print "  target_notrace : { . = .; libzebrasim-greybox.a(no_trace __llvm_prf*) }"; \
print "  target_data : { . = .; __start_target_data = .; libzebrasim-greybox.a(.data); __stop_target_data = .; }"; \
print "  target_nvm : { . = .; __start_target_nvm = .; libzebrasim-greybox.a(nvm); __stop_target_nvm = .; }"; \
print $0; \
next \
}{print $0}'
