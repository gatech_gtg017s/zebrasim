// zebrasim -- simulated CCID target for zebrafuzz fuzzer
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>

#define DELAY_ENABLED (0)

// 100 - (350+100)us matches actual hardware device's speed
#define DELAY_MIN_US (100)
#define DELAY_RANGE_US (350)

//#define DELAY_MIN_US (0)
//#define DELAY_RANGE_US (1)

#define TS_TO_MS(ts) (((uint64_t)(ts)->tv_sec * 1000L) + ((uint64_t)(ts)->tv_nsec / 1000 / 1000))
#define TS_TO_S(ts)  (((uint64_t)(ts)->tv_sec) + ((uint64_t)(ts)->tv_nsec / 1000 / 1000 / 1000))

#ifdef WITH_ABORT
#define ABORT(idx) abort()
#else
static void dump_stats(void);
#define ABORT_COUNT (10)
static uint64_t abort_stats[128] = {0};
static struct timespec fuzz_start;
static struct timespec abort_stats_ts[128] = {0};
#define ABORT(idx) do {                                               \
    int i, aborts = 0;                                                \
    abort_stats[idx]++;                                               \
    if (abort_stats[idx] == 1) {                                      \
      clock_gettime(CLOCK_REALTIME, &abort_stats_ts[idx]);            \
      fprintf(stderr, "INFO: hit abort() #%u at %s:%u (%lu times)\n", \
              idx, __FILE__, __LINE__, abort_stats[idx]);             \
    }                                                                 \
    for (i = 0; i < ABORT_COUNT; i++) {                               \
      if (abort_stats[i] > 0) aborts++;                               \
    }                                                                 \
    if (aborts == ABORT_COUNT) {                                      \
      fprintf(stderr, "INFO: all bugs reached, exiting!");            \
      dump_stats();                                                   \
      abort();                                                        \
    }                                                                 \
  } while(0)
#endif

#if (!defined(GREYBOX) && !defined(BLACKBOX)) || (defined(GREYBOX) && defined(BLACKBOX))
#error Simulator must be built in exactly one of GREYBOX or BLACKBOX configurations.
#endif

#define APDU_MAX_LEN (4096)
#define CCID_MAX_RESPONSES (8)

__attribute__((used,section ("no_trace"))) static uint64_t apdu_stats[4] = {0};

static uint8_t bulk_response[1024*64] __attribute__((used,section ("no_trace")));
static uint8_t apdubuf[APDU_MAX_LEN] __attribute__((used,section ("no_trace")));

static uint8_t nvmbuf[128] __attribute__((used,section ("nvm")));
static uint8_t nvm_checkpoints[4] __attribute__((used,section ("nvm")));
static uint8_t test_data[] = {0x11,0x22,0x33,0x44,0xaa,0xbb,0xcc,0xdd};
static uint64_t a1 = 0, a2 = 0;
#if defined(BLACKBOX)
static uint8_t checkpoints[4];
#endif

#if defined(WITH_MAIN)
#define MPRINT(...) printf(__VA_ARGS__);
#else
#define MPRINT(...)
#endif

#pragma pack(push, 1)
typedef struct {
  uint8_t bMessageType;
  uint32_t dwLength;
  uint8_t bSlot;
  uint8_t bSeq;
  uint8_t bStatus;
  uint8_t bError;
  uint8_t bChainParameter;
} ccid_header_t;
#pragma pack(pop)

typedef struct {
  uint8_t buf[sizeof(ccid_header_t)+sizeof(apdubuf)];
  uint8_t valid;
  uint16_t len;
} ccid_response_t;

// allow a few CCID responses to stack up
static ccid_response_t ccid_response[CCID_MAX_RESPONSES] __attribute__((used,section ("no_trace")));

#pragma pack(push, 1)
typedef struct {
  uint8_t class;
  uint8_t ins;
  uint8_t p1;
  uint8_t p2;
  uint16_t lc;
  uint16_t le;
} apdu_t;
#pragma pack(pop)

typedef enum {
  APDU_STATUS_IDLE,
  APDU_STATUS_MORE_CAPDU,
  APDU_STATUS_ACTIVE,
  APDU_STATUS_RAPDU,
} apdu_status_t;

typedef struct {
  apdu_t apdu;
  uint16_t offset;
  uint8_t status;
  uint8_t retlen;
} apdu_state_t;

typedef enum {
  APPLET_NONE = 0,
  APPLET_CONFIG,
  APPLET_SMARTCARD,
  APPLET_STORAGE,
  APPLET_SECRET,
  APPLET_SECRET2,
} applet_t;

__attribute__((used,section ("no_trace"))) static apdu_state_t apdu_state;

static uint8_t ccid_powered_on = 0;
static applet_t cur_applet = APPLET_NONE;
static uint8_t some_global = 0;
static uint8_t secret2 = 0;

static void apdu_reset(void) {
  apdu_state.status = APDU_STATUS_IDLE;
  apdu_state.offset = 0;
}

static int smartcard_dispatch(apdu_t *apdu, uint8_t *data, size_t len) {
  // default response: INS_NOT_SUPPORTED
  data[0] = 0x6d;
  data[1] = 0x00;
  switch (apdu->ins) {
  case 0x01:
    MPRINT("sc: reply\n");
    memset(data, 0xaa, 5);
    if (apdu->p1 == 0x01) {
      data[0] = 0xbb;
    }
    if (apdu->p2 == 0x02) {
      data[1] = 0xcc;
    }
    data[5] = 0x90;
    data[6] = 0x00;
    return 7;
  case 0x02:
    MPRINT("sc: inc\n");
    if (apdu->p1 == apdu->p2) {
      data[0] = test_data[1];
      data[1] = 0x90;
      data[2] = 0x00;
      test_data[1]++;
      return 3;
    }
    else {
      data[0] = 0x6a;
      data[1] = 0x80;
    }
    break;
  case 0x03:
    MPRINT("sc: abort\n");
    if (apdu->p1 == 5 && test_data[1] > 100) {
      // a crash exclusively in hardware, but with an edge visible in
      // simulation
#if defined(BLACKBOX)
      // disabled, finding it too often
      //printf("ABORT0: %u %u\n", apdu->p1, test_data[1]);
      ABORT(0);
#endif
      test_data[1] = 0;
    }
    break;
  case 0x04:
    MPRINT("sc: branchy\n");
    switch (apdu->p1) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      data[0] = 0x90;
      break;
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
      data[0] = 0x69;
      data[1] = 0x86;
      break;
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
      if (apdu->p2 < 30) {
        data[0] = 0x69;
        data[1] = 0x83;
      }
      else if (apdu->p2 < 60) {
        data[0] = 0x69;
        data[1] = 0x84;
      }
      else {
        data[0] = 0x69;
        data[1] = 0x85;
      }
      break;
#if defined(BLACKBOX)
  case 16:
    checkpoints[0] = 1;
    data[0] = 0x90;
    break;
  case 17:
    checkpoints[1] = 1;
    data[0] = 0x90;
    break;
#endif
    default:
      data[0] = 0x6a;
      data[1] = 0x80;
      break;
    }
    break;
  default:
    break;
  }
  return 2;
}

static int storage_dispatch(apdu_t *apdu, uint8_t *data, size_t len) {
  // default response: INS_NOT_SUPPORTED
  data[0] = 0x6d;
  data[1] = 0x00;
  switch (apdu->ins) {
  case 120:
    MPRINT("storage: 120\n");
    if (apdu->p1 != 0) {
      memset(nvmbuf, apdu->p1, sizeof(nvmbuf));
    }
    else if (apdu->p2 == 0) {
      memset(nvmbuf, 0, sizeof(nvmbuf));
    }
    data[0] = 0x90;
    break;
  case 121:
    MPRINT("storage: 121\n");
    nvmbuf[apdu->p1 % sizeof(nvmbuf)] = apdu->p2;
    data[0] = 0x90;
    break;
  case 122:
    MPRINT("storage: 122\n");
    if (apdu->p1 > 128 && apdu->p2 < 128) {
      memmove(nvmbuf, nvmbuf + 1, sizeof(nvmbuf) - 1);
      nvmbuf[sizeof(nvmbuf)-1] = 0;
    }
    data[0] = 0x90;
    break;
  case 123:
    MPRINT("storage: abort\n");
    if (nvmbuf[1] > nvmbuf[0] &&
        nvmbuf[2] > nvmbuf[1] &&
        nvmbuf[3] > nvmbuf[2]) {
      memset(nvmbuf, 0, sizeof(nvmbuf));
#if defined(BLACKBOX)
      //printf("ABORT1: %u %u %u %u\n", nvmbuf[0], nvmbuf[1], nvmbuf[2], nvmbuf[3]);
      ABORT(1);
#endif
    }
    data[0] = 0x90;
    break;
  case 124:
    MPRINT("storage: memcpy\n");
    if (apdu->lc > 0) {
      memcpy(nvmbuf, data, apdu->lc > sizeof(nvmbuf) ? sizeof(nvmbuf) : apdu->lc);
      data[0] = 0x90;
    }
    else {
      data[0] = 0x6a;
      data[1] = 0x80;
    }
    break;
  case 125:
#if defined(BLACKBOX)
    if (checkpoints[0] && checkpoints[1] && checkpoints[2] && checkpoints[3]) {
      //printf("ABORT6: %u %u %u %u\n", checkpoints[0], checkpoints[1], checkpoints[2], checkpoints[3]);
      memset(checkpoints, 0, sizeof(checkpoints));
      ABORT(6);
    }
#endif
    data[0] = 0x90;
    break;
  case 200:
    if (apdu->p1 == 0x1f) {
      if (apdu->lc > 50 && apdubuf[2] == apdu->lc) {
        nvm_checkpoints[0] = 1;
      }
    }
    data[0] = 0x90;
    break;
  case 201:
    if (apdu->p1 == 0x79) {
      if (apdu->lc > 100 && apdu->lc == apdu->p2) {
        nvm_checkpoints[1] = 1;
      }
    }
    data[0] = 0x90;
    break;
  case 220:
    if ((apdu->p1 & 0xf0) == 0xf0 && (apdu->p2 & 0x06) == 0x06) {
      nvm_checkpoints[2] = 1;
    }
    data[0] = 0x90;
    break;
#if defined(BLACKBOX)
  case 230:
    if (apdu->p1 == 0xaa) {
      nvm_checkpoints[3] = 1;
    }
    data[0] = 0x90;
    break;
  case 231:
    if (nvm_checkpoints[0] && nvm_checkpoints[1] && nvm_checkpoints[2] && nvm_checkpoints[3]) {
      //printf("ABORT7: %u %u %u %u\n", nvm_checkpoints[0], nvm_checkpoints[1], nvm_checkpoints[2], nvm_checkpoints[3]);
      memset(nvm_checkpoints, 0, sizeof(nvm_checkpoints));
      ABORT(7);
    }
    data[0] = 0x90;
    break;
#endif
  default:
    MPRINT("storage: unknown\n");
    break;
  }
  return 2;
}

static int config_dispatch(apdu_t *apdu, uint8_t *data, size_t len) {
  // default response: INS_NOT_SUPPORTED
  data[0] = 0x6d;
  data[1] = 0x00;

  switch (apdu->ins) {
  case 0x01: // echo
    if (len > APDU_MAX_LEN - 2) {
      // error if not enough space for status word
      data[0] = 0x67;
      data[1] = 0x00;
      return 2;
    }
    // just append status word and return
    data[len] = 0x90;
    data[len + 1] = 0x00;
    MPRINT("config: echo %s\n", data);
    return len+2;
  case 0x02: // bug
    MPRINT("config: abort\n");
#if defined(BLACKBOX)
    // a crash exclusively in hardware
    if (apdu->p1 == test_data[0] && apdu->p2 == test_data[0]) {
      //printf("ABORT2: %u %u %u\n", test_data[0], apdu->p1, apdu->p2);
      ABORT(2);
    }
    data[0] = 0x90;
#endif
    break;
  case 0x03: // modify data
    MPRINT("config: mod data\n");
    if (apdu->p1 < 128) {
      test_data[0]++;
    }
    else {
      test_data[0]--;
    }
    data[len] = 0x90;
    data[len + 1] = 0x00;
    break;
  case 0x04: // modify bss
    MPRINT("config: mod bss\n");
    if (apdu->p1 == 0) {
      some_global++;
    }
    else if (apdu->p1 == 1) {
      some_global--;
    }
    else if (apdu->p1 == 2 && apdu->p2 == 2) {
      some_global = 0;
    }
    data[len] = 0x90;
    data[len + 1] = 0x00;
    break;
  case 0x05: // bug
    MPRINT("config: static abort\n");
#if defined(BLACKBOX)
    static uint64_t abt = 0;
    // a crash exclusively in hardware
    if (apdu->p1 == 0xaa && apdu->p2 == 0xbb) {
      abt++;
      if (abt > 10 == 0) {
        //printf("ABORT4: %u %u %u\n", apdu->p1, apdu->p2, abt);
        ABORT(4);
      }
    }
#endif
    data[0] = 0x90;
    break;
  case 0x06:
    if (apdu->p1 == 0x01 && apdu->p2 == 0x02 && apdu->lc == 4) {
      a1++;
    }
    data[0] = 0x90;
    break;
  case 0x07:
    if (apdu->p1 == 0x03 && apdu->p2 == 0x04 && apdu->lc == 4) {
      a2++;
    }
    data[0] = 0x90;
    break;
  case 0x08:
#if defined(BLACKBOX)
    if (apdu->lc == 5 && a1 > 100 && a2 > 100) {
      // depends on INS 6 and 7, which are sim-interesting, but INS 8 is not
      //printf("ABORT5: %u %u %u\n", apdu->lc, a1, a2);
      ABORT(5);
    }
    data[0] = 0x90;
#endif
    break;
#if defined(BLACKBOX)
  case 0x09:
    checkpoints[2] = 1;
    data[0] = 0x90;
    break;
  case 0x0a:
    checkpoints[3] = 1;
    data[0] = 0x90;
    break;
#endif
  case 0x41:
    if (apdu->lc == 0) {
      secret2 = 1;
    }
    data[0] = 0x90;
    break;
  default:
    MPRINT("config: unknown\n");
    break;
  }
  return 2;
}

static int secret_dispatch(apdu_t *apdu, uint8_t *data, size_t len) {
  static int key_set = 0, email_set = 0, name_set = 0;

  switch (apdu->ins) {
  case 0x01:
    if (apdu->p1 == 0xa1 && apdu->p2 == 0x00) {
      if (data[0] == 0x5f && data[1] == 32 && apdu->lc == 34) {
        key_set = 1;
      }
    }
    data[0] = 0x90;
    break;
  case 0x02:
    if (apdu->p1 == 0xa2 && apdu->lc > 10 && key_set) {
      name_set = 1;
    }
    data[0] = 0x90;
    break;
  case 0x03:
    if (apdu->p1 == 0xa3 && apdu->lc > 20 && name_set) {
      if (memchr(data, '@', len) != NULL) {
        email_set = 1;
      }
    }
    data[0] = 0x90;
    break;
  case 0x04:
    if (apdu->p1 == 0xa4 && apdu->lc > 128) {
      if (key_set && email_set && name_set) {
#if defined(BLACKBOX)
        ABORT(8);
#endif
      }
    }
    data[0] = 0x90;
    break;
  default:
    MPRINT("secret: unknown\n");
    data[0] = 0x6d;
    data[1] = 0x00;
    break;
  }
  return 2;
}

static int secret2_dispatch(apdu_t *apdu, uint8_t *data, size_t len) {
  static int loaded = 0;
  static int counter = 0;

#if defined(BLACKBOX)
  //abort();
#endif

  switch (apdu->ins) {
  case 0xf0:
    counter++;
    data[0] = 0x90;
    break;
  case 0xf1:
    loaded = 1;
    data[0] = 0x90;
    break;
  case 0x00:
    if (apdu->p1 == 0x01 && apdu->p2 == 0x02 && apdu->lc == 0) {
      //printf("ABORT9: %u %u\n", loaded, counter);
      if (loaded && counter > 10) {
#if defined(BLACKBOX)
        ABORT(9);
#endif
        counter = loaded = 0;
      }
    }
    data[0] = 0x90;
    break;
  default:
    MPRINT("secret2: unknown\n");
    data[0] = 0x6d;
    data[1] = 0x00;
    break;
  }
  return 2;
}

static int apdu_dispatch(void) {
  uint16_t sw;
  if (apdu_state.apdu.ins == 0xa4 &&
      apdu_state.apdu.p1 == 0x04 &&
      apdu_state.apdu.p2 == 0x00) {
    sw = 0x6a82;
    apdu_state.retlen = 2;
    // applet selection
    if (apdu_state.apdu.lc == 8 &&
        memcmp(apdubuf, "\xa0\x00\x00\x11\x22\x33\x44\x55", 8) == 0) {
      MPRINT("select: config\n");
      cur_applet = APPLET_CONFIG;
      sw = 0x9000;
    }
    else if (apdu_state.apdu.lc == 8 &&
             memcmp(apdubuf, "\xa0\x00\x00\xaa\xbb\xcc\xdd\xee", 8) == 0) {
      MPRINT("select: smartcard\n");
      cur_applet = APPLET_SMARTCARD;
      sw = 0x9000;
    }
    else if (apdu_state.apdu.lc == 16 &&
             memcmp(apdubuf, "\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16) == 0) {
      MPRINT("select: secret\n");
      cur_applet = APPLET_SECRET;
      sw = 0x9000;
    }
    else if (secret2 &&
             apdu_state.apdu.lc == 16 &&
             memcmp(apdubuf, "\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01", 16) == 0) {
      MPRINT("select: secrets\n");
      cur_applet = APPLET_SECRET2;
      sw = 0x9000;
    }
    else if (apdu_state.apdu.lc == 12 &&
             memcmp(apdubuf, "\xa0\x00\x00\xaa\xbb\xcc\xdd\xee\x00\x11\x22\x33", 12) == 0) {
      MPRINT("select: storage\n");
      cur_applet = APPLET_STORAGE;
      sw = 0x9000;
    }
    else {
      MPRINT("select: unknown\n");
      // leave previous applet selected
    }
    apdubuf[0] = (sw >> 8) & 0xff;
    apdubuf[1] = sw & 0xff;
    apdu_state.status = APDU_STATUS_RAPDU;
    return 0;
  }

  switch (cur_applet) {
  case APPLET_CONFIG:
    apdu_state.retlen = config_dispatch(&apdu_state.apdu, apdubuf, apdu_state.apdu.lc);
    break;
  case APPLET_SMARTCARD:
    apdu_state.retlen = smartcard_dispatch(&apdu_state.apdu, apdubuf, apdu_state.apdu.lc);
    break;
  case APPLET_STORAGE:
    apdu_state.retlen = storage_dispatch(&apdu_state.apdu, apdubuf, apdu_state.apdu.lc);
    break;
  case APPLET_SECRET:
    apdu_state.retlen = secret_dispatch(&apdu_state.apdu, apdubuf, apdu_state.apdu.lc);
    break;
  case APPLET_SECRET2:
    apdu_state.retlen = secret2_dispatch(&apdu_state.apdu, apdubuf, apdu_state.apdu.lc);
    break;
  default:
    apdubuf[0] = 0x6d;
    apdubuf[1] = 0x00;
    apdu_state.retlen = 2;
    break;
  }
  apdu_stats[cur_applet]++;

  apdu_state.status = apdu_state.retlen > 0 ? APDU_STATUS_RAPDU : APDU_STATUS_IDLE;
  return 0;
}

static int apdu_parse_header(const uint8_t *data, size_t size) {
  uint8_t header_size = 5;

  if (size < 5 ||
      (data[4] == 0x00 && size > 5 && size < 7)) {
    // smaller than an APDU header
    return -1;
  }

  apdu_state.apdu.class = data[0];
  apdu_state.apdu.ins = data[1];
  apdu_state.apdu.p1 = data[2];
  apdu_state.apdu.p2 = data[3];

  if (data[4] == 0x00 && size > 5) {
    header_size = 7;
    apdu_state.apdu.lc = (data[5] << 8) | data[6];
  }
  else {
    apdu_state.apdu.lc = data[4];
  }

  if (apdu_state.apdu.lc > APDU_MAX_LEN) {
    // command too large
    return -1;
  }

  if (apdu_state.apdu.lc + header_size > size) {
    // incomplete message, more to come
    apdu_state.status = APDU_STATUS_MORE_CAPDU;
    memcpy(apdubuf, data + header_size, size - header_size);
    apdu_state.offset = size - header_size;
  }
  else {
    // complete message
    apdu_state.status = APDU_STATUS_ACTIVE;
    memcpy(apdubuf, data + header_size, apdu_state.apdu.lc);
    apdu_state.offset = apdu_state.apdu.lc;
  }
  return 0;
}

static int apdu_parse(const uint8_t *data, size_t size) {
  switch (apdu_state.status) {
  case APDU_STATUS_IDLE:
    if (apdu_parse_header(data, size)) {
      apdu_reset();
      return -1;
    }
    break;
  case APDU_STATUS_MORE_CAPDU: {
    uint16_t len = size > apdu_state.apdu.lc - apdu_state.offset ?
      apdu_state.apdu.lc - apdu_state.offset : size;
    memcpy(apdubuf + apdu_state.offset, data, len);
    apdu_state.offset += len;
    if (apdu_state.offset == apdu_state.apdu.lc) {
      apdu_state.status = APDU_STATUS_ACTIVE;
    }
  }
    break;
  default:
    // busy...
    return -1;
  }

  if (apdu_state.status == APDU_STATUS_ACTIVE) {
    return apdu_dispatch();
  }
  return 0;
}

static int ccid_parse(const uint8_t *data, size_t size) {
  uint16_t offset = 0;
  uint8_t resp_idx = 0;
  ccid_header_t *in_hdr;
  ccid_header_t *out_hdr;
  ccid_response_t *resp;

  // allow multiple CCID commands in a single input, and process them serially
  while (1) {
    in_hdr = (ccid_header_t*)(data + offset);
    resp = &ccid_response[resp_idx];
    out_hdr = (ccid_header_t*)resp->buf;

#if defined(BLACKBOX)
    if (DELAY_ENABLED) {
      // artificial relay for command input
      uint16_t rand_us = (rand() % DELAY_RANGE_US) + DELAY_MIN_US; // 2-22ms
      usleep(rand_us);
    }
#endif

    // stop processing on invalid length
    if (in_hdr->dwLength + sizeof(ccid_header_t) > size) {
      break;
    }

    // default response to a slot-status response
    out_hdr->bMessageType = 0x81;
    out_hdr->dwLength = 0;
    out_hdr->bSlot = in_hdr->bSlot;
    out_hdr->bSeq = in_hdr->bSeq;
    out_hdr->bStatus = 0;
    out_hdr->bError = 0;
    out_hdr->bChainParameter = 0;
    resp->len = sizeof(ccid_header_t);

    switch (in_hdr->bMessageType) {
    case 0x62: {// PC_to_Rdr_IccPowerOn
      out_hdr->bMessageType = 0x80;
      out_hdr->dwLength = 10;
      // dummy ATR
      memcpy(resp->buf + sizeof(ccid_header_t), "\x11\x22\x33\x44\x55\xaa\xbb\xcc\xdd\xee", 10);
      resp->len += 10;
      ccid_powered_on = 1;
    }
      break;
    case 0x63: // PC_to_Rdr_IccPowerOff
      ccid_powered_on = 0;
      break;
    case 0x6f: // PC_to_Rdr_XfrBlock
      if (!ccid_powered_on) {
        out_hdr->bStatus = 0x40; // error
        out_hdr->bError = 0xE0; // CMD_SLOT_BUSY
        break;
      }
#if defined(BLACKBOX)
      // some specific low-level hardware-only crash
      if (in_hdr->dwLength == 25 && in_hdr->bSeq == 10) {
        ABORT(3);
      }
#endif
      // error if returns error, or completes without a response
      if (apdu_parse(data + offset + sizeof(ccid_header_t), in_hdr->dwLength) != 0 ||
          (apdu_state.status != APDU_STATUS_MORE_CAPDU &&
           (apdu_state.status == APDU_STATUS_IDLE ||
            apdu_state.retlen < 2))) {
        out_hdr->bStatus = 0x40; // error
        out_hdr->bError = 0x00; // CMD_NOT_SUPPORTED
        break;
      }
      else if (apdu_state.status == APDU_STATUS_RAPDU) {
        out_hdr->bMessageType = 0x80;
        out_hdr->dwLength = apdu_state.retlen;
        memcpy(resp->buf + sizeof(ccid_header_t), apdubuf, apdu_state.retlen);
        resp->len += apdu_state.retlen;
        apdu_state.status = APDU_STATUS_IDLE;
      }
      break;
    case 0x72: // PC_to_Rdr_Abort
      apdu_reset();
      break;
    default:
      out_hdr->bStatus = 0x40; // error
      out_hdr->bError = 0x00; // CMD_NOT_SUPPORTED
      break;
    }

    // wait for CCID response to be consumed
    if (resp->len > 0) {
      resp->valid = 1;
      resp_idx++;
      if (resp_idx >= CCID_MAX_RESPONSES) {
        // no space for more responses
        break;
      }
    }

    // move to the next CCID packet (if any)
    offset += in_hdr->dwLength + sizeof(ccid_header_t);

    // out of bytes, finished processing
    if (offset >= size) {
      break;
    }
  }

  return 0;
}

static int ccid_read_response(uint8_t *buf, size_t max_size) {
  ccid_response_t *resp;
  uint16_t len;
  int i;

  for (i = 0; i < CCID_MAX_RESPONSES; i++) {
    resp = &ccid_response[i];
    if (resp->valid) {
#if defined(BLACKBOX)
      if (DELAY_ENABLED) {
        // artificial relay for response output
        uint16_t rand_us = (rand() % DELAY_RANGE_US) + DELAY_MIN_US; // 2-22ms
        usleep(rand_us);
      }
#endif

      len = resp->len <= max_size ? resp->len : max_size;
      memcpy(buf, resp->buf, len);
      if (len == resp->len) {
        resp->valid = 0;
      }
      else {
        // move data down in buffer and leave it valid.  next call will read it.
        memmove(resp->buf, resp->buf + len, resp->len - len);
        resp->len -= len;
      }
      return len;
    }
  }
  return 0;
}

static int icc_power_on(void) {
  uint8_t buf[64];
  if (ccid_parse((const uint8_t*)"\x62\x00\x00\x00\x00\x00\x01\x00\x00\x00", 10) < 0) {
    fprintf(stderr, "WARNING: icc power on didn't parse\n");
    return -1;
  }
  if (ccid_read_response(buf, sizeof(buf)) != 20) {
    fprintf(stderr, "WARNING: icc power on didn't respond\n");
    return -1;
  }
  return 0;
}

static int ccid_abort(void) {
  uint8_t buf[64];
  if (ccid_parse((const uint8_t*)"\x72\x00\x00\x00\x00\x00\x01\x00\x00\x00", 10) < 0) {
    fprintf(stderr, "WARNING: ccid abort didn't parse\n");
    return -1;
  }
  if (ccid_read_response(buf, sizeof(buf)) != 10) {
    fprintf(stderr, "WARNING: ccid abort didn't respond\n");
    return -1;
  }
  return 0;
}


#if defined(BLACKBOX)
int LLVMFuzzerTestOneInputHw(const uint8_t *data, size_t size) {
#else
int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
#endif
  uint16_t apdu_len, data_len;
  uint16_t offset, readlen;

  if (size < 10 || size > sizeof(bulk_response))
    return -1;

  if (size > 3072+10) {
    return -1;
  }

#if defined(WITH_MAIN)
  static int corpus_idx = 0;
  char fname[128];
  int fd;
  mkdir("./corpus", 0770);
  snprintf(fname, sizeof(fname), "./corpus/corpus-%02u", corpus_idx);
  fname[sizeof(fname)-1] = 0;
  if ((fd = open(fname, O_CREAT | O_WRONLY, 0660)) < 0) {
    fprintf(stderr, "ERROR: failed to open corpus file: %s\n", fname);
  }
  if (write(fd, data, size) != size) {
    fprintf(stderr, "ERROR: failed to write corpus file: %s\n", fname);
  }
  close(fd);
  corpus_idx++;
#endif

  apdu_len = size - 10;
  memcpy(bulk_response, data, size);
  bulk_response[1] = (apdu_len >>  0) & 0xff;
  bulk_response[2] = (apdu_len >>  8) & 0xff;
  bulk_response[3] = (apdu_len >> 16) & 0xff;
  bulk_response[4] = (apdu_len >> 24) & 0xff;

  if (apdu_len >= 5 && apdu_len <= 255) {
    // short Lc
    data_len = apdu_len - 5;
    bulk_response[14] = data_len;
  }
  else if (apdu_len >= 7) {
    // long Lc
    data_len = apdu_len - 7;
    bulk_response[14] = 0;
    bulk_response[15] = (data_len >> 8) & 0xff;
    bulk_response[16] = data_len & 0xff;
  }

#if defined(BLACKBOX)
  //printf(" - libusb xfer (len: %ld)\n", size);
#endif

  ccid_parse(bulk_response, size);
  offset = 0;
  while ((readlen = ccid_read_response(bulk_response + offset, sizeof(bulk_response) - offset)) > 0) {
    offset += readlen;
  }
#if defined(BLACKBOX)
  //fprintf(stdout, " - ccid response: %u bytes\n", offset);
  //fflush(stdout);
#endif

  if (ccid_abort() < 0) abort();
  if (data[0] == 0x63) {
    if (icc_power_on() < 0) abort();
  }
  return 0;
}

static void dump_stats(void) {
  int i;
  fprintf(stderr, "\nFUZZ HARNESS STATS:\n");
  fprintf(stderr, " * APDUs:\n");
  for (i = 0; i < 4; i++) {
    fprintf(stderr, "  - %i: %lu\n", i, apdu_stats[i]);
  }
#ifndef WITH_ABORT
  uint64_t start_s = TS_TO_S(&fuzz_start);
  uint64_t end_s;
  struct timespec fuzz_end;
  clock_gettime(CLOCK_REALTIME, &fuzz_end);
  end_s = TS_TO_S(&fuzz_end);

  fprintf(stderr, " * ABORTs:\n");
  for (i = 0; i < ABORT_COUNT; i++) {
    fprintf(stderr, "  - %i: %lu (%lu s)\n", i, abort_stats[i], TS_TO_S(&abort_stats_ts[i]) - start_s);
  }
#endif
  fprintf(stderr, "Total time: %lu s\n", end_s - start_s);
}

static void sighandler(int sig) {
  dump_stats();
}

#if defined(BLACKBOX)
int LLVMFuzzerInitializeHw(int *argc, char ***argv) {
#else
int LLVMFuzzerInitialize(int *argc, char ***argv) {
#endif
  srand(time(NULL));
  atexit(dump_stats);
  signal(SIGUSR1, sighandler);
  if (icc_power_on() < 0) abort();
  if (ccid_abort() < 0) abort();
  clock_gettime(CLOCK_REALTIME, &fuzz_start);
  return 0;
}

#if defined(WITH_MAIN)
static void hexdump(const char *str, const uint8_t *buf, size_t buflen) {
  int i;
  if (str)
    fprintf(stderr, "%s:\n", str);
  for (i = 0; i < buflen; i++) {
    if (i && i % 32 == 0)
      fprintf(stderr, "\n");
    fprintf(stderr, "%02x ", buf[i]);
  }
  fprintf(stderr, "\n");
}

static int echo(void) {
  uint8_t buf[64];
  if (ccid_parse((const uint8_t*)"\x6f\x0d\x00\x00\x00\x00\x01\x00\x00\x00\x00\xa4\x04\x00\x08\xa0\x00\x00\x11\x22\x33\x44\x55", 10+5+8) < 0) {
    fprintf(stderr, "WARNING: select didn't parse\n");
    return -1;
  }
  if (ccid_read_response(buf, sizeof(buf)) != 12) {
    fprintf(stderr, "WARNING: select didn't respond\n");
    return -1;
  }
  hexdump("SEL", buf, 12);

  if (ccid_parse((const uint8_t*)"\x6f\x0d\x00\x00\x00\x00\x02\x00\x00\x00\x00\x01\x00\x00\x08\xaa\xbb\xcc\xdd\xee\xff\x11\x22", 10+5+8) < 0) {
    fprintf(stderr, "WARNING: echo didn't parse\n");
    return -1;
  }
  if (ccid_read_response(buf, sizeof(buf)) != 20) {
    fprintf(stderr, "WARNING: echo didn't respond\n");
    return -1;
  }
  hexdump("ECHO", buf, 20);

  return 0;
}

int main(int argc, char **argv) {
  LLVMFuzzerInitialize(NULL, NULL);
  if (icc_power_on() < 0) abort();
  if (ccid_abort() < 0) abort();
  if (echo() < 0) abort();

  uint8_t cmd_sel_config[] = "\x6f\x0d\x00\x00\x00\x00\x01\x00\x00\x00\x00\xa4\x04\x00\x08\xa0\x00\x00\x11\x22\x33\x44\x55";
  LLVMFuzzerTestOneInput(cmd_sel_config, sizeof(cmd_sel_config)-1);

  uint8_t cmd_echo[] = "\x6f\x0d\x00\x00\x00\x00\x02\x00\x00\x00\x00\x01\x00\x00\x08\xaa\xbb\xcc\xdd\xee\xff\x11\x22";
  LLVMFuzzerTestOneInput(cmd_echo, sizeof(cmd_echo)-1);

  uint8_t cmd_abort[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x02\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_abort, sizeof(cmd_abort)-1);

  uint8_t cmd_abort_aa[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x02\xaa\xaa\x00";
  LLVMFuzzerTestOneInput(cmd_abort_aa, sizeof(cmd_abort_aa)-1);

  uint8_t cmd_abort_bb[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x02\xbb\xbb\x00";
  LLVMFuzzerTestOneInput(cmd_abort_bb, sizeof(cmd_abort_bb)-1);

  uint8_t cmd_data[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x03\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_data, sizeof(cmd_data)-1);

  uint8_t cmd_bss[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x04\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_bss, sizeof(cmd_bss)-1);

  uint8_t cmd_undef[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x05\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_undef, sizeof(cmd_undef)-1);

  uint8_t cmd_sel_sc[] = "\x6f\x0d\x00\x00\x00\x00\x01\x00\x00\x00\x00\xa4\x04\x00\x08\xa0\x00\x00\xaa\xbb\xcc\xdd\xee";
  LLVMFuzzerTestOneInput(cmd_sel_sc, sizeof(cmd_sel_sc)-1);

  uint8_t cmd_reply[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x01\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_reply, sizeof(cmd_reply)-1);

  uint8_t cmd_inc[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x02\x02\x02\x00";
  LLVMFuzzerTestOneInput(cmd_inc, sizeof(cmd_inc)-1);

  uint8_t cmd_abort2[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x03\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_abort2, sizeof(cmd_abort2)-1);

  uint8_t cmd_branch[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x04\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_branch, sizeof(cmd_branch)-1);

  uint8_t cmd_sel_storage[] = "\x6f\x11\x00\x00\x00\x00\x01\x00\x00\x00\x00\xa4\x04\x00\x0c\xa0\x00\x00\xaa\xbb\xcc\xdd\xee\x00\x11\x22\x33";
  LLVMFuzzerTestOneInput(cmd_sel_storage, sizeof(cmd_sel_storage)-1);

  uint8_t cmd_120[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x078\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_120, sizeof(cmd_120)-1);
  uint8_t cmd_121[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x079\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_121, sizeof(cmd_121)-1);
  uint8_t cmd_122[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x07a\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_122, sizeof(cmd_122)-1);
  uint8_t cmd_abort3[] = "\x6f\x05\x00\x00\x00\x00\x02\x00\x00\x00\x00\x07b\x00\x00\x00";
  LLVMFuzzerTestOneInput(cmd_abort3, sizeof(cmd_abort3)-1);
  uint8_t cmd_cpy[] = "\x6f\x09\x00\x00\x00\x00\x02\x00\x00\x00\x00\x07c\x00\x00\x04\x01\x02\x03\x04";
  LLVMFuzzerTestOneInput(cmd_cpy, sizeof(cmd_cpy)-1);

  return 0;
}
#endif
